<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Setting;
use Illuminate\Http\Request;

class DepartmentsController extends Controller
{
    //

    public function index()
    {
        $departments = Department::all();

        if (!$departments) {
            return response()->json(['error' => 'Data not found'], 404);
        }

        return response()->json($departments, 200);
    }
}
