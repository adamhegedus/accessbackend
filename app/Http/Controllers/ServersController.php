<?php

namespace App\Http\Controllers;

use App\Models\Server;
use Carbon\Carbon;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

use App\Http\Requests;

class ServersController extends Controller
{
    //

    public function index()
    {
        $servers = Server::with('tags')->get();

        return $servers;
    }


    public function show($id)
    {
        $server = Server::where('pingdom_id', '=', $id)->first();

        if (!$server) {
            return response()->json(['error' => 'Server not found'], 404);
        }

        return response()->json($server, 200);
    }

}
