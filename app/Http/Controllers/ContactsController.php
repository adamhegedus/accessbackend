<?php

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Models\Contact;

class ContactsController extends Controller
{
    //

    public function index()
    {
        $contacts = Contact::with('department')->get();

        return $contacts;
    }


    public function show($id)
    {
        $contact = Contact::where('id', '=', $id)->with('department')->first();

        if (!$contact) {
            return response()->json(['error' => 'Contact not found'], 404);
        }

        return response()->json($contact, 200);
    }


    public function avatar($id)
    {
        $contact = Contact::where('id', '=', $id)->with('department')->first();

        $b64 = \Avatar::create($contact->first_name . " " . $contact->last_name)->toBase64();

        $img = explode(',', $b64);

        return response(base64_decode($img[1]), 200)
            ->header('Content-Type', 'image/png');
    }


    public function store(Request $request)
    {

    }


    public function update(Request $request, $id)
    {

    }


    public function destroy(Request $request, $id)
    {

    }
}
