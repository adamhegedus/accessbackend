<?php

namespace App\Http\Controllers;

use App\Models\Alert;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class AlertsController extends Controller
{
    //

    public function index(Request $request)
    {

        $monthCount = $request->input('months', 12);

        $start_date = Carbon::create()->subMonths($monthCount);

        $alerts = Alert::with(['contact' => function($q) {
            $q->select('first_name', 'last_name', 'id');
        }])->where('created_at', '>', $start_date)->get();

        return $alerts;

    }


    public function store(Request $request)
    {
        $alert_text = $request->input('alert.text');
        $alert_hash = $request->header('hash');
        $alert_isEmergency = $request->input('alert.isEmergency', false);

        if ($alert_hash == hash('sha256', $alert_text . "[ENERGYCAPINCSOFTWARE]"))
        {
            $alert = new Alert();
            $alert->alert_body = $alert_text;
            $alert->$alert_isEmergency;
            $alert->contact_id = 18;
            $alert->save();

            return response()->json($alert, 200);
        }

        return response()->json(['error' => 'Invalid format'], 406);
    }


    public function destroy($id)
    {

    }
}
