<?php

namespace App\Console\Commands;

use App\Models\DailySummary;
use App\Models\Server;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class GetDailyPingdomData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'access:get_summary_data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $servers = Server::all();

        foreach ($servers as $server) {
            $today = Carbon::create(null, null, null, 8, 0, 0, 'America/New_York');
            $processDate = Carbon::create(null, null, 1, 8, 0, 0, 'America/New_York')->subMonths(3);

            while($processDate->lt($today)) {
                $startDate = Carbon::create($processDate->year, $processDate->month, $processDate->day, 8, 0, 0, 'America/New_York');
                $endDate = Carbon::create($processDate->year, $processDate->month, $processDate->day, 16, 0, 0, 'America/New_York');

                if ($this->shouldSaveData($startDate, $server)) {
                    $this->getSaveData($startDate, $endDate, $server);
                }

                $processDate->addDay();
            }
        }
    }


    private function shouldSaveData(Carbon $date, Server $server)
    {
        if (($date->dayOfWeek == Carbon::SATURDAY) && ($date->dayOfWeek == Carbon::SUNDAY)) {
            return false;
        }

        if ($server->dailySummaries()->where('summary_date', '=', $date->toDateString())->first()) {
            return false;
        } else {
            return true;
        }
    }


    private function getSaveData(Carbon $startDate, Carbon $endDate, Server $server)
    {
        $uri = "/api/2.0/summary.average/" . $server->pingdom_id . "?includeuptime=true&from=" . $startDate->timestamp . "&to=" . $endDate->timestamp;

        $client = new Client([
                                 'base_uri' => 'https://api.pingdom.com',
                                 'headers' => ['App-Key' => 'h68qvbz9vtvb8p6q7rzwf3fyzde69blf']
                             ]);

        $response = $client->request('GET', $uri, ['auth' => ['osc@energycap.com', 'faser1217']]);
        $body = $response->getBody();

        $json = json_decode($body, true);

        $totalDown = $json["summary"]["status"]["totaldown"];

        $totalSeconds = 60 * 60 * 12;

        $percentDown = number_format((($totalSeconds - $totalDown)/$totalSeconds) * 100);

        $data = new DailySummary();
        $data->summary_date = $startDate;
        $data->response_time = $json["summary"]["responsetime"]["avgresponse"];
        $data->downtime_seconds = $totalDown;
        $data->uptime_percentage = $percentDown;
        $data->server()->associate($server);
        $data->save();
    }
}
