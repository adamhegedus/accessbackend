<?php

namespace App\Console\Commands;

use App\Models\Server;
use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Console\Command;
use GuzzleHttp\Client;

class GetPingdomData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'access:get_pingdom_data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pull Data from Pingdom and store it in the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        $client = new Client([
            'base_uri' => 'https://api.pingdom.com',
            'headers' => ['App-Key' => 'h68qvbz9vtvb8p6q7rzwf3fyzde69blf']
        ]);

        $response = $client->request('GET', '/api/2.0/checks?include_tags=true', ['auth' => ['osc@energycap.com', 'faser1217']]);

        $body = $response->getBody();

        $json= json_decode($body, true);

        foreach ($json['checks'] as $check) {
            if ($server = Server::where('pingdom_id', '=', $check['id'])->first()) {

                // server in db, update the record
                $this->info('Updating server ' . $check['name']);

                if ($server->status == 'up'  && $check['status'] == 'down') {
                    // server went down, send an alert
                } else if ($server->status == 'down'  && $check['status'] == 'up') {
                    // server back up, send an alert
                } else if ($server->status == 'down'  && $check['status'] == 'down') {
                    // server still downs
                }

                $server->name = $check['name'];
                $server->hostname = $check['hostname'];
                $server->status = $check['status'];
                $server->lastErrorTime = Carbon::createFromTimestamp($check['lasterrortime']);
                $server->lastTestTime = Carbon::createFromTimestamp($check['lasttesttime']);
                $server->lastResponseTime = $check['lastresponsetime'];
                $server->save();

                $tags = [];

                foreach ($check['tags'] as $server_tag) {
                    if ($tag = Tag::where('name', '=', $server_tag['name'])->first()) {
                        $tags[] = $tag->id;
                    } else {
                        $tag = new Tag();
                        $tag->name = $server_tag['name'];
                        $tag->save();

                        $tags[] = $tag->id;
                    }
                }

                $server->tags()->sync($tags);

            } else {
                // server not in database create it
                $this->info('Saving server ' . $check['name']);

                if ($check['status'] == 'down') {
                    // server went down, send an alert
                }

                $server = new Server();
                $server->name = $check['name'];
                $server->pingdom_id = $check['id'];
                $server->hostname = $check['hostname'];
                $server->status = $check['status'];
                $server->lastErrorTime = Carbon::createFromTimestamp($check['lasterrortime']);
                $server->lastTestTime = Carbon::createFromTimestamp($check['lasttesttime']);
                $server->lastResponseTime = $check['lastresponsetime'];
                $server->location = 'State College';
                $server->save();

                $tags = [];

                foreach ($check['tags'] as $server_tag) {
                    if ($server_tag['name'] == 'pittsburgh') {
                        $server->location = 'Pittsburgh';
                        $server->save();
                    }

                    if ($tag = Tag::where('name', '=', $server_tag['name'])->first()) {
                        $tags[] = $tag->id;
                    } else {
                        $tag = new Tag();
                        $tag->name = $server_tag['name'];
                        $tag->save();

                        $tags[] = $tag->id;
                    }
                }

                $server->tags()->sync($tags);
            }
        }
    }
}
