<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
    //
    protected $appends = array('sender');


    public function contact()
    {
        return $this->belongsTo('App\Models\Contact');
    }

    public function getSenderAttribute()
    {
        return $this->contact->first_name . " " . $this->contact->last_name;
    }

}
