<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //


    public function servers()
    {
        return $this->belongsToMany('App\Models\Server', 'server_tags', 'server_id', 'tag_id');
    }
}
