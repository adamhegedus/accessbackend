<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //

    public function department()
    {
        return $this->belongsTo('App\Models\Department');
    }

    public function alerts()
    {
        return $this->hasMany('App\Models\Alert');
    }

    public function devices()
    {
        return $this->hasMany('App\Models\Device');
    }
}
