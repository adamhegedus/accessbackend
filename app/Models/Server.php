<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    protected $appends = array('avg_response', 'uptime_percentage', 'total_downtime');

    //
    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'server_tags', 'tag_id', 'server_id');
    }

    public function dailySummaries()
    {
        return $this->hasMany('App\Models\DailySummary');
    }

    public function getAvgResponseAttribute()
    {
        $endDate = Carbon::create(null, null, 1, 8, 0, 0, 'America/New_York');
        $startDate = $endDate->copy()->subMonths(3);

        return number_format($this->dailySummaries()->where('summary_date', '>=', $startDate)->where('summary_date', '<=', $endDate)->avg('response_time'));
    }

    public function getTotalDowntimeAttribute()
    {
        $endDate = Carbon::create(null, null, 1, 8, 0, 0, 'America/New_York');
        $startDate = $endDate->copy()->subMonths(3);

        return $this->convert_to_string_time($this->dailySummaries()->where('summary_date', '>=', $startDate)->where('summary_date', '<=', $endDate)->sum('downtime_seconds'));
    }

    public function getUptimePercentageAttribute()
    {
        $endDate = Carbon::create(null, null, 1, 8, 0, 0, 'America/New_York');
        $startDate = $endDate->copy()->subMonths(3);
        $totalDays = $startDate->diffInDays($endDate);
        $totalSeconds = $totalDays * (12 * 60 * 60);

        $down_seconds = $this->dailySummaries()->where('summary_date', '>=', $startDate)->where('summary_date', '<=', $endDate)->sum('downtime_seconds');
        return number_format((($totalSeconds - $down_seconds)/$totalSeconds) * 100, 2);
    }


    private function convert_to_string_time($num_seconds)
    {
        $seconds = $num_seconds % 60;
        $min = floor( $num_seconds / 60 );
        if( $min == 0 )
            return "{$seconds} sec";
        else
            return "{$min} min {$seconds} sec";
    }

}
