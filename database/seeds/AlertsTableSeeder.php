<?php

use Illuminate\Database\Seeder;

class AlertsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adam = \App\Models\Contact::where('last_name', '=', 'Hegedus')->first();

        if ($adam) {
            DB::table('alerts')->insert(['alert_body'=>'Welcome to EnergyCAP OPS Access.  This will be your source for information about what\'s happening in Operations.  Thanks for installing!', 'isEmergency'=>0, 'created_at'=>'2014-04-16 17:00:00', 'updated_at'=>'2014-04-16 17:00:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Reports are currently not working on staging. We are looking into the problem now. Will send update when fixed.', 'isEmergency'=>0, 'created_at'=>'2014-05-22 11:46:00', 'updated_at'=>'2014-05-22 11:46:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Reports are now working on staging.', 'isEmergency'=>0, 'created_at'=>'2014-05-22 11:57:00', 'updated_at'=>'2014-05-22 11:57:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'PRO DB 002 will be down this evening for two hours starting at 7pm for catalyst conversions. Clients have been notified. ', 'isEmergency'=>0, 'created_at'=>'2014-04-18 13:50:00', 'updated_at'=>'2014-04-18 13:50:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'We\'re experiencing issues with PRODFILE01 this morning. Working on it. ', 'isEmergency'=>0, 'created_at'=>'2014-05-19 09:01:00', 'updated_at'=>'2014-05-19 09:01:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Prodfile01 is back up and running. If you still have issues accessing your mapped drives you should reboot your computer. ', 'isEmergency'=>0, 'created_at'=>'2014-05-19 09:17:00', 'updated_at'=>'2014-05-19 09:17:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Something has happened with services at the regent server room. This has caused us to loose connectivity at the new building. OPS is en route to Regent. Thanks for your patience. ', 'isEmergency'=>0, 'created_at'=>'2014-06-13 09:55:00', 'updated_at'=>'2014-06-13 09:55:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Just a reminder that the building move continues today. All internal and external facing office servers will be taken down at noon today. This includes staging. #TMinus2hours #Fireinthehole', 'isEmergency'=>0, 'created_at'=>'2014-06-14 10:18:00', 'updated_at'=>'2014-06-14 10:18:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Houston, this is Aquarius, catch you on the flip side. #serversdown', 'isEmergency'=>0, 'created_at'=>'2014-06-14 12:25:00', 'updated_at'=>'2014-06-14 12:25:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'If anyone is close to sandy drive in a little less than an hour, we could use some muscle unloading the truck...  #radiocheck', 'isEmergency'=>0, 'created_at'=>'2014-06-14 14:30:00', 'updated_at'=>'2014-06-14 14:30:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Headed to sandy drive now...', 'isEmergency'=>0, 'created_at'=>'2014-06-14 15:29:00', 'updated_at'=>'2014-06-14 15:29:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'After a 17 hour grind yesterday (and this morning) AJ and Jeff are almost done... #dedication', 'isEmergency'=>0, 'created_at'=>'2014-06-15 11:33:00', 'updated_at'=>'2014-06-15 11:33:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Hello Houston... this is Odyssey... it\'s good to see you again. #serversup', 'isEmergency'=>0, 'created_at'=>'2014-06-15 23:46:00', 'updated_at'=>'2014-06-15 23:46:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Office365 mail services appear to be in degraded state. OPS is opening a ticket with Microsoft. ', 'isEmergency'=>0, 'created_at'=>'2014-06-24 10:37:00', 'updated_at'=>'2014-06-24 10:37:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Microsoft is aware of the mail issue and is getting slammed with complaints. More info when we have it...', 'isEmergency'=>0, 'created_at'=>'2014-06-24 12:40:00', 'updated_at'=>'2014-06-24 12:40:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Email should be working normally now. Please let us know tomorrow if there are still any lingering issues.', 'isEmergency'=>0, 'created_at'=>'2014-06-24 22:26:00', 'updated_at'=>'2014-06-24 22:26:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Data center is back online after upgrades. ', 'isEmergency'=>0, 'created_at'=>'2015-01-17 13:52:00', 'updated_at'=>'2015-01-17 13:52:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Staging response time is slow, looking into it. ', 'isEmergency'=>0, 'created_at'=>'2015-04-08 08:34:00', 'updated_at'=>'2015-04-08 08:34:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Having some sluggishness with staging again. We\'re working on it. ', 'isEmergency'=>0, 'created_at'=>'2015-04-09 12:04:00', 'updated_at'=>'2015-04-09 12:04:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'First person to YELL out "YAHTZEE!", wins a prize...', 'isEmergency'=>0, 'created_at'=>'2015-05-26 12:00:00', 'updated_at'=>'2015-05-26 12:00:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Staging is currently having some issues. We\'re working on it. ', 'isEmergency'=>0, 'created_at'=>'2015-05-30 19:07:36', 'updated_at'=>'2015-05-30 19:07:36', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Staging, support and Jira are currently having some issues. We\'re working on it. ', 'isEmergency'=>0, 'created_at'=>'2015-05-30 19:25:23', 'updated_at'=>'2015-05-30 19:25:23', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Staging, support and Jira are currently having some issues. We\'re working on it. ', 'isEmergency'=>0, 'created_at'=>'2015-05-30 19:32:44', 'updated_at'=>'2015-05-30 19:32:44', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'All services have been restored. ', 'isEmergency'=>0, 'created_at'=>'2015-05-30 21:28:09', 'updated_at'=>'2015-05-30 21:28:09', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Office server and Internet issues have been resolved. ', 'isEmergency'=>0, 'created_at'=>'2015-07-27 08:18:41', 'updated_at'=>'2015-07-27 08:18:41', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Office servers (staging, Jira, Support) are having issues again, we are aware and working on the issue. ', 'isEmergency'=>0, 'created_at'=>'2015-08-11 06:43:11', 'updated_at'=>'2015-08-11 06:43:11', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Office services have been restored. ', 'isEmergency'=>0, 'created_at'=>'2015-08-11 08:39:17', 'updated_at'=>'2015-08-11 08:39:17', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'We had to bring Jira down due to an email loop. It should be back up soon. We apologize if you received a lot of emails...', 'isEmergency'=>0, 'created_at'=>'2015-09-03 17:01:27', 'updated_at'=>'2015-09-03 17:01:27', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Door codes have been changed. New code is 17395', 'isEmergency'=>0, 'created_at'=>'2015-09-20 14:31:07', 'updated_at'=>'2015-09-20 14:31:07', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Power issues on Sandy Drive. Office servers are shutting down. ', 'isEmergency'=>0, 'created_at'=>'2016-01-10 15:31:43', 'updated_at'=>'2016-01-10 15:31:43', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Having some issues with authentication and email. Ops is working on it and it will be resolved shortly. ', 'isEmergency'=>0, 'created_at'=>'2016-05-15 15:34:00', 'updated_at'=>'2016-05-15 15:34:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'It appears GoToMeeting is having significant service interruptions. We\'re monitoring it.', 'isEmergency'=>0, 'created_at'=>'2016-06-17 10:29:00', 'updated_at'=>'2016-06-17 10:29:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Our normal voip provider is having routing issues so we have switched to our backup. Please let us know if you have any phone call issues.', 'isEmergency'=>0, 'created_at'=>'2016-06-21 15:14:00', 'updated_at'=>'2016-06-21 15:14:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Quarterly meeting starts in 3 minutes. ', 'isEmergency'=>0, 'created_at'=>'2016-06-30 12:42:00', 'updated_at'=>'2016-06-30 12:42:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'We are experiencing possible internet issues in State College. This was unexpected and hopefully will be resolved shortly. We will let everyone know when things are back up.', 'isEmergency'=>0, 'created_at'=>'2016-08-13 14:11:00', 'updated_at'=>'2016-08-13 14:11:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'We are experiencing possible internet issues in State College. This was unexpected and hopefully will be resolved shortly. We will let everyone know when things are back up.', 'isEmergency'=>0, 'created_at'=>'2016-08-13 14:11:00', 'updated_at'=>'2016-08-13 14:11:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'It appears the alarm system at the office was zapped by lightning. It currently will not arm. It will be fixed this week. ', 'isEmergency'=>0, 'created_at'=>'2016-08-13 22:42:00', 'updated_at'=>'2016-08-13 22:42:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Due to weather conditions, today\'s company picnic is cancelled. Lunch will be provided at the office tomorrow around noon.', 'isEmergency'=>0, 'created_at'=>'2016-08-21 13:14:00', 'updated_at'=>'2016-08-21 13:14:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'We are experiencing an internet outage. We will send another message when things are working again.', 'isEmergency'=>0, 'created_at'=>'2016-08-31 15:46:00', 'updated_at'=>'2016-08-31 15:46:00', 'contact_id'=>$adam->id]);
            DB::table('alerts')->insert(['alert_body'=>'Internet is back!', 'isEmergency'=>0, 'created_at'=>'2016-08-31 16:07:00', 'updated_at'=>'2016-08-31 16:07:00', 'contact_id'=>$adam->id]);
        }
    }
}
