<?php

use Illuminate\Database\Seeder;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $now = new \Carbon\Carbon();
        DB::table('departments')->insert(['id' => 1, 'name' => 'Administration', 'created_at' => $now, 'updated_at' => $now]);
        DB::table('departments')->insert(['id' => 2, 'name' => 'Operations', 'created_at' => $now, 'updated_at' => $now]);
        DB::table('departments')->insert(['id' => 3, 'name' => 'Development', 'created_at' => $now, 'updated_at' => $now]);
        DB::table('departments')->insert(['id' => 4, 'name' => 'Sales and Marketing', 'created_at' => $now, 'updated_at' => $now]);
        DB::table('departments')->insert(['id' => 5, 'name' => 'Implementation Solutions', 'created_at' => $now, 'updated_at' => $now]);
        DB::table('departments')->insert(['id' => 6, 'name' => 'Customer Service', 'created_at' => $now, 'updated_at' => $now]);
        DB::table('departments')->insert(['id' => 7, 'name' => 'Human Resources', 'created_at' => $now, 'updated_at' => $now]);
        DB::table('departments')->insert(['id' => 8, 'name' => 'Bill CAPture', 'created_at' => $now, 'updated_at' => $now]);
    }
}
