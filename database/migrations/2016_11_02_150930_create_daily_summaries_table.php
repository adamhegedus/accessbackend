<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailySummariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('daily_summaries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('server_id');
            $table->date('summary_date');
            $table->integer('response_time');
            $table->integer('downtime_seconds');
            $table->double('uptime_percentage');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('daily_summaries');
    }
}
