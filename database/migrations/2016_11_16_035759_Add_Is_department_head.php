<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsDepartmentHead extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('contacts', function ($table) {
            $table->boolean('isDeptHead')->default(0);
        });

        \App\Models\Contact::where('last_name','=', 'Heinz')->update(['isDeptHead'=>1]);
        \App\Models\Contact::where('last_name','=', 'Ohlson')->update(['isDeptHead'=>1]);
        \App\Models\Contact::where('last_name','=', 'Ulmer')->update(['isDeptHead'=>1]);
        \App\Models\Contact::where('last_name','=', 'Booz')->update(['isDeptHead'=>1]);
        \App\Models\Contact::where('last_name','=', 'Behringer')->update(['isDeptHead'=>1]);
        \App\Models\Contact::where('last_name','=', 'Hegedus')->where('first_name','=', 'Adam')->update(['isDeptHead'=>1]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('contacts', function ($table) {
            $table->dropColumn('isDeptHead');
        });
    }
}
