<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAlertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('alerts');

        //
        Schema::create('alerts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contact_id');
            $table->text('alert_body');
            $table->boolean('isEmergency')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('alerts');

        //
        Schema::create('alerts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sender');
            $table->string('alert_body', 160);
            $table->boolean('isEmergency');
            $table->timestamps();
        });
    }
}
