<?php

return array(

    'IOS'     => array(
        'environment' =>env('IOS_ENV', 'development'),
        'certificate' =>env('IOS_KEY_PATH', app_path() . '/Keys/com.energycap.access_push_development.pem'),
        'passPhrase'  =>'password',
        'service'     =>'apns'
    ),
    'ANDROID' => array(
        'environment' =>env('ANDROID_ENV', 'production'),
        'apiKey'      =>env('ANDROID_KEY_PATH', app_path() . '/Keys/com.energycap.your_api.pem'),
        'service'     =>'gcm'
    )

);