<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::get('/user', function (Request $request) {
//    return $request->user();
//})->middleware('auth:api');

Route::group(['prefix' => 'v1', 'middleware' => 'auth0.jwt'], function () {

    Route::get('alerts', 'AlertsController@index');
    Route::post('alert', 'AlertsController@store');

    Route::get('servers', 'ServersController@index');
    Route::get('servers/{pingdom_id}', 'ServersController@show');

    Route::get('contacts', 'ContactsController@index');
    Route::get('contact/{contact_id}', 'ContactsController@show');
    Route::post('contact', 'ContactsController@store');
    Route::put('contact/{contact_id}', 'ContactsController@update');
    Route::delete('contacts/{contact_id}', 'ContactsController@destroy');
    Route::get('contact/{contact_id}/avatar', 'ContactsController@avatar');

    Route::get('departments', 'DepartmentsController@index');

});


Route::group(['prefix' => 'vx', 'middleware' => 'api'], function () {

    Route::get('alerts', 'AlertsController@index');
    Route::post('alert', 'AlertsController@store');

    Route::get('servers', 'ServersController@index');
    Route::get('servers/{pingdom_id}', 'ServersController@show');

    Route::get('contacts', 'ContactsController@index');
    Route::get('contact/{contact_id}', 'ContactsController@show');
    Route::post('contact', 'ContactsController@store');
    Route::put('contact/{contact_id}', 'ContactsController@update');
    Route::delete('contacts/{contact_id}', 'ContactsController@destroy');

    Route::get('contact/{contact_id}/avatar', 'ContactsController@avatar');

    Route::get('departments', 'DepartmentsController@index');

});

